-- Model for EVE Data, based on EVE database dump.

-- Type of data     Data type
-- ISK (Currency)   NUMERIC(14, 2)
-- Volume           NUMERIC(11, 2)
-- Position         DOUBLE PRECISION

-- Template:

/*
DROP TABLE IF EXISTS table_name;

CREATE TABLE table_name
(
    name VARCHAR(256),

    original_id INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT table_name_pkey PRIMARY KEY (name),
    CONSTRAINT table_name_original_id_key UNIQUE (original_id)
);
*/

DROP VIEW IF EXISTS
    corporation_statistics,
    connected_regions,
    connected_constellations,
    station_planetary_systems;

DROP TABLE IF EXISTS
    planets,
    stars,
    stations,
    regions,
    constellations,
    planetary_systems,
    planetary_system_jumps,
    factions,
    corporations,
    corporation_investors,
    moons,
    ships,
    ship_types,
    characters,
    character_ships,
    items,
    skills,
    attribute_types,
    attributes,
    skill_attributes;

DROP FUNCTION IF EXISTS stations_planet_xor_moon_check();
DROP FUNCTION IF EXISTS stations_get_planetary_system_name(planet_name VARCHAR(256));
DROP FUNCTION IF EXISTS all_update_updated_on();

CREATE TABLE corporations
(
    name VARCHAR(256),

    original_id INTEGER,

    planetary_system_name VARCHAR(256),
    faction_name VARCHAR(256),
    friend_corporation_name VARCHAR(256),
    enemy_corporation_name VARCHAR(256),

    size CHARACTER(1),
    extent CHARACTER(1),
    public_shares BIGINT,
    initial_price NUMERIC(14, 2),
    min_security DOUBLE PRECISION,
    scattered BOOLEAN,
    size_factor DOUBLE PRECISION,
    description VARCHAR(4096),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT corporations_pkey PRIMARY KEY (name),
    CONSTRAINT corporations_original_id_key UNIQUE (original_id)
);

CREATE TABLE corporation_investors
(
    corporation_name VARCHAR(256) NOT NULL,
    investor_corporation_name VARCHAR(256) NOT NULL,

    shares INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT corporation_investors_pkey PRIMARY KEY (corporation_name, investor_corporation_name)
);

CREATE TABLE factions
(
    name VARCHAR(256),

    original_id INTEGER,

    planetary_system_name VARCHAR(256) NOT NULL,
    corporation_name VARCHAR(256) NOT NULL,
    militia_corporation_name VARCHAR(256),
    race_name VARCHAR(256),

    description VARCHAR(4096),
    size_factor DOUBLE PRECISION,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT factions_pkey PRIMARY KEY (name),
    CONSTRAINT factions_original_id_key UNIQUE (original_id)
);

CREATE TABLE regions
(
    name VARCHAR(256),

    original_id INTEGER,

    faction_name VARCHAR(256),

    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    x_minimum DOUBLE PRECISION,
    x_maximum DOUBLE PRECISION,
    y_minimum DOUBLE PRECISION,
    y_maximum DOUBLE PRECISION,
    z_minimum DOUBLE PRECISION,
    z_maximum DOUBLE PRECISION,
    radius DOUBLE PRECISION,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT regions_pkey PRIMARY KEY (name),
    CONSTRAINT regions_original_id_key UNIQUE (original_id)
);

CREATE TABLE constellations
(
    name VARCHAR(256),

    original_id INTEGER,

    region_name VARCHAR(256),
    faction_name VARCHAR(256),

    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    x_minimum DOUBLE PRECISION,
    x_maximum DOUBLE PRECISION,
    y_minimum DOUBLE PRECISION,
    y_maximum DOUBLE PRECISION,
    z_minimum DOUBLE PRECISION,
    z_maximum DOUBLE PRECISION,
    radius DOUBLE PRECISION,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT constellations_pkey PRIMARY KEY (name),
    CONSTRAINT constellations_original_id_key UNIQUE (original_id)
);

CREATE TABLE planetary_systems
(
    name VARCHAR(256) NOT NULL,

    original_id INTEGER,

    constellation_name VARCHAR(256),
    sun_type_name VARCHAR(256),
    faction_name VARCHAR(256),

    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    x_minimum DOUBLE PRECISION,
    x_maximum DOUBLE PRECISION,
    y_minimum DOUBLE PRECISION,
    y_maximum DOUBLE PRECISION,
    z_minimum DOUBLE PRECISION,
    z_maximum DOUBLE PRECISION,
    luminosity DOUBLE PRECISION,
    security DOUBLE PRECISION,
    radius DOUBLE PRECISION,
    security_class VARCHAR(2),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT planetary_systems_pkey PRIMARY KEY (name),
    CONSTRAINT planetary_systems_original_id_key UNIQUE (original_id)
);

CREATE TABLE planetary_system_jumps
(
    from_planetary_system_name VARCHAR(256),
    to_planetary_system_name VARCHAR(256),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT planetary_system_jumps_pkey PRIMARY KEY (
        from_planetary_system_name,
        to_planetary_system_name
    )
);

CREATE TABLE stars
(
    name VARCHAR(256),

    original_id INTEGER,

    planetary_system_name VARCHAR(256) NOT NULL,
    star_type_name VARCHAR(256) NOT NULL,

    temperature INTEGER,
    spectral_class VARCHAR(16),
    luminosity DOUBLE PRECISION,
    age DOUBLE PRECISION,
    life DOUBLE PRECISION,
    radius DOUBLE PRECISION,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT stars_pkey PRIMARY KEY (name),
    CONSTRAINT stars_original_id_key UNIQUE (original_id)
);

CREATE TABLE planets
(
    name VARCHAR(256),

    original_id INTEGER,

    star_name VARCHAR(256) NOT NULL,
    planet_type_name VARCHAR(256) NOT NULL,

    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    radius DOUBLE PRECISION,
    temperature DOUBLE PRECISION,
    orbit_radius DOUBLE PRECISION,
    orbit_period DOUBLE PRECISION,
    eccentricity DOUBLE PRECISION,
    mass_dust DOUBLE PRECISION,
    mass_gas DOUBLE PRECISION,
    density DOUBLE PRECISION,
    surface_gravity DOUBLE PRECISION,
    escape_velocity DOUBLE PRECISION,
    rotation_rate DOUBLE PRECISION,
    locked BOOLEAN,
    pressure DOUBLE PRECISION,
    orbit_position INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT planets_pkey PRIMARY KEY (name),
    CONSTRAINT planets_original_id_key UNIQUE (original_id)
);

CREATE TABLE moons
(
    name VARCHAR(256),

    original_id INTEGER,

    planet_name VARCHAR(256) NOT NULL,

    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    radius DOUBLE PRECISION,
    temperature DOUBLE PRECISION,
    orbit_radius DOUBLE PRECISION,
    orbit_period DOUBLE PRECISION,
    eccentricity DOUBLE PRECISION,
    mass_dust DOUBLE PRECISION,
    mass_gas DOUBLE PRECISION,
    density DOUBLE PRECISION,
    surface_gravity DOUBLE PRECISION,
    escape_velocity DOUBLE PRECISION,
    rotation_rate DOUBLE PRECISION,
    locked BOOLEAN,
    pressure DOUBLE PRECISION,
    orbit_position INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT moons_pkey PRIMARY KEY (name),
    CONSTRAINT moons_original_id_key UNIQUE (original_id)
);

CREATE TABLE stations
(
    name VARCHAR(256) NOT NULL,

    original_id INTEGER,

    operation_name VARCHAR(256),
    planet_name VARCHAR(256),
    moon_name VARCHAR(256),
    corporation_name VARCHAR(256) NOT NULL,

    security INTEGER,
    docking_cost_per_volume NUMERIC(14, 2),
    max_ship_volume_dockable NUMERIC(11, 2),
    office_rental_cost NUMERIC(14, 2),
    x DOUBLE PRECISION,
    y DOUBLE PRECISION,
    z DOUBLE PRECISION,
    reprocessing_efficiency DOUBLE PRECISION,
    reprocessing_stations_take DOUBLE PRECISION,
    reprocessing_hangar_flag INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT stations_pkey PRIMARY KEY (name),
    CONSTRAINT stations_original_id_key UNIQUE (original_id)
);

CREATE TABLE ship_types
(
    name VARCHAR(256) NOT NULL,

    original_id INTEGER,

    description VARCHAR(4096),
    use_base_price BOOLEAN,
    published BOOLEAN,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT ship_types_pkey PRIMARY KEY (name),
    CONSTRAINT ship_types_original_id_key UNIQUE (original_id)
);

CREATE TABLE items
(
    name VARCHAR(256),

    original_id INTEGER,

    race_name VARCHAR(256),
    market_group_name VARCHAR(256) NOT NULL,

    description VARCHAR(4096),
    mass DOUBLE PRECISION,
    volume DOUBLE PRECISION,
    capacity DOUBLE PRECISION,
    portion_size INTEGER,
    base_price NUMERIC(14, 2),
    published BOOLEAN,
    chance_of_duplicating DOUBLE PRECISION,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT items_pkey PRIMARY KEY (name),
    CONSTRAINT items_original_id_key UNIQUE (original_id)
);

CREATE TABLE attributes
(
    attribute_type_name VARCHAR(256),
    item_name VARCHAR(256),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT attributes_pkey PRIMARY KEY (attribute_type_name, item_name)
);

CREATE TABLE attribute_types
(
    name VARCHAR(256),

    original_id INTEGER,

    category_name VARCHAR(256),

    description VARCHAR(4096),
    published BOOLEAN,
    display_name VARCHAR(256),
    stackable BOOLEAN,
    high_is_good BOOLEAN,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT attribute_types_pkey PRIMARY KEY (name),
    CONSTRAINT attribute_types_original_id_key UNIQUE (original_id)
);

CREATE TABLE skills
(
    item_name VARCHAR(256),

    rank INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT skills_pkey PRIMARY KEY (item_name)
);

CREATE TABLE skill_attributes
(
    attribute_type_name VARCHAR(256),
    attribute_item_name VARCHAR(256),

    skill_item_name VARCHAR(256),

    level INTEGER,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT skill_attributes_pkey PRIMARY KEY (attribute_type_name, attribute_item_name)
);

CREATE TABLE characters
(
    name VARCHAR(256),

    race_name VARCHAR(256),
    corporation_name VARCHAR(256),
    current_station_name VARCHAR(256),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT characters_pkey PRIMARY KEY (name)
);

CREATE TABLE ships
(
    item_name VARCHAR(256),

    ship_type_name VARCHAR(256) NOT NULL,

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT ships_pkey PRIMARY KEY (item_name)
);

CREATE TABLE character_ships
(
    name VARCHAR(256),

    character_name VARCHAR(256),
    ship_name VARCHAR(256),

    created_on TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_on TIMESTAMP DEFAULT NULL,

    CONSTRAINT character_ships_pkey PRIMARY KEY (name)
);

-- Adding foreign keys

-- corporations

ALTER TABLE corporations ADD
    CONSTRAINT corporations_planetary_system_name_fkey
    FOREIGN KEY (planetary_system_name)
    REFERENCES planetary_systems (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE corporations ADD
    CONSTRAINT corporations_faction_name_fkey
    FOREIGN KEY (faction_name)
    REFERENCES factions (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE corporations ADD
    CONSTRAINT corporations_friend_corporation_name_fkey
    FOREIGN KEY (friend_corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE corporations ADD
    CONSTRAINT corporations_enemy_corporation_name_fkey
    FOREIGN KEY (enemy_corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- corporation_investors

ALTER TABLE corporation_investors ADD
    CONSTRAINT corporation_investors_corporation_name_fkey
    FOREIGN KEY (corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE corporation_investors ADD
    CONSTRAINT corporation_investors_investor_corporation_name_fkey
    FOREIGN KEY (investor_corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- factions

ALTER TABLE factions ADD
    CONSTRAINT factions_planetary_system_name_fkey
    FOREIGN KEY (planetary_system_name)
    REFERENCES planetary_systems (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE factions ADD
    CONSTRAINT factions_corporation_name_fkey
    FOREIGN KEY (corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE factions ADD
    CONSTRAINT factions_militia_corporation_name_fkey
    FOREIGN KEY (militia_corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- regions

ALTER TABLE regions ADD
    CONSTRAINT regions_faction_name_fkey
    FOREIGN KEY (faction_name)
    REFERENCES factions (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- constellations

ALTER TABLE constellations ADD
    CONSTRAINT constellations_faction_name_fkey
    FOREIGN KEY (faction_name)
    REFERENCES factions (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE constellations ADD
    CONSTRAINT constellations_region_name_fkey
    FOREIGN KEY (region_name)
    REFERENCES regions (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- planetary_systems

ALTER TABLE planetary_systems ADD
    CONSTRAINT planetary_systems_constellation_name_fkey
    FOREIGN KEY (constellation_name)
    REFERENCES constellations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE planetary_systems ADD
    CONSTRAINT planetary_systems_faction_name_fkey
    FOREIGN KEY (faction_name)
    REFERENCES factions (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- planetary_system_jumps

ALTER TABLE planetary_system_jumps ADD
    CONSTRAINT planetary_system_jumps_from_planetary_system_name_fkey
    FOREIGN KEY (from_planetary_system_name)
    REFERENCES planetary_systems (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE planetary_system_jumps ADD
    CONSTRAINT planetary_system_jumps_to_planetary_system_name_fkey
    FOREIGN KEY (to_planetary_system_name)
    REFERENCES planetary_systems (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- stars

ALTER TABLE stars ADD
    CONSTRAINT stars_planetary_system_name_fkey
    FOREIGN KEY (planetary_system_name)
    REFERENCES planetary_systems (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- planets

ALTER TABLE planets ADD
    CONSTRAINT planets_star_name_fkey
    FOREIGN KEY (star_name)
    REFERENCES stars (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- moons

ALTER TABLE moons ADD
    CONSTRAINT moons_star_name_fkey
    FOREIGN KEY (planet_name)
    REFERENCES planets (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- stations

ALTER TABLE stations ADD
    CONSTRAINT stations_planet_name_fkey
    FOREIGN KEY (planet_name)
    REFERENCES planets (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE stations ADD
    CONSTRAINT stations_moon_name_fkey
    FOREIGN KEY (moon_name)
    REFERENCES moons (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE stations ADD
    CONSTRAINT stations_corporation_name_fkey
    FOREIGN KEY (corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- items

-- attributes

ALTER TABLE attributes ADD
    CONSTRAINT attributes_item_name_fkey
    FOREIGN KEY (item_name)
    REFERENCES items (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE attributes ADD
    CONSTRAINT attributes_attribute_type_name_fkey
    FOREIGN KEY (attribute_type_name)
    REFERENCES attribute_types (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- attribute_types

-- skills

ALTER TABLE skills ADD
    CONSTRAINT skills_item_name_fkey
    FOREIGN KEY (item_name)
    REFERENCES items (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- skill_attributes

ALTER TABLE skill_attributes ADD
    CONSTRAINT skill_attributes_attribute_type_name_attribute_item_name_fkey
    FOREIGN KEY (attribute_type_name, attribute_item_name)
    REFERENCES attributes (attribute_type_name, item_name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE skill_attributes ADD
    CONSTRAINT skill_attributes_skill_item_name_fkey
    FOREIGN KEY (skill_item_name)
    REFERENCES skills (item_name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- characters

ALTER TABLE characters ADD
    CONSTRAINT characters_corporation_name_fkey
    FOREIGN KEY (corporation_name)
    REFERENCES corporations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE characters ADD
    CONSTRAINT characters_current_station_name_fkey
    FOREIGN KEY (current_station_name)
    REFERENCES stations (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- character_ships

ALTER TABLE character_ships ADD
    CONSTRAINT  character_ships_character_name_fkey
    FOREIGN KEY (character_name)
    REFERENCES characters (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE character_ships ADD
    CONSTRAINT  character_ships_ship_name_fkey
    FOREIGN KEY (ship_name)
    REFERENCES ships (item_name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- ships

ALTER TABLE ships ADD
    CONSTRAINT ships_ship_type_name_fkey
    FOREIGN KEY (ship_type_name)
    REFERENCES ship_types (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE ships ADD
    CONSTRAINT ships_item_name_fkey
    FOREIGN KEY (item_name)
    REFERENCES items (name)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- Functions

CREATE FUNCTION stations_planet_xor_moon_check() RETURNS trigger AS $stations_planet_xor_moon_check$
    BEGIN
        IF NEW.planet_name IS NOT NULL AND NEW.moon_name IS NOT NULL THEN
            RAISE EXCEPTION 'planet_name and moon_name cannot be both not null';
        END IF;

        IF NEW.planet_name IS NULL AND NEW.moon_name IS NULL THEN
            RAISE EXCEPTION 'planet_name and moon_name cannot be both null';
        END IF;

        RETURN NEW;
    END;
$stations_planet_xor_moon_check$ LANGUAGE plpgsql;

CREATE FUNCTION stations_get_planetary_system_name(station_name VARCHAR(256)) RETURNS VARCHAR(256)
    AS $stations_get_planetary_system_name$
    DECLARE
        station RECORD;
        planet_name VARCHAR(256);
        planetary_system_name VARCHAR(256);
    BEGIN
        SELECT s.planet_name, s.moon_name INTO station FROM stations AS s WHERE s.name = station_name;

        IF station.planet_name IS NOT NULL THEN
            planet_name := station.planet_name;
        ELSE
            SELECT m.planet_name INTO planet_name FROM moons AS m WHERE m.name = station.moon_name;
        END IF;

        SELECT ps.name INTO planetary_system_name FROM planetary_systems
            INNER JOIN planets ON (planets.name = planet_name)
            INNER JOIN stars ON (stars.name = planets.star_name)
            INNER JOIN planetary_systems AS ps ON (ps.name = stars.planetary_system_name);

        RETURN planetary_system_name;
    END;
$stations_get_planetary_system_name$ LANGUAGE plpgsql;

CREATE FUNCTION all_update_updated_on() RETURNS trigger AS $all_update_updated_on$
    BEGIN
        NEW.updated_on = NOW();

        RETURN NEW;
    END;
$all_update_updated_on$ LANGUAGE plpgsql;

-- Triggers

CREATE CONSTRAINT TRIGGER stations_planet_xor_moon_check AFTER INSERT OR UPDATE ON stations
    FOR EACH ROW EXECUTE PROCEDURE stations_planet_xor_moon_check();

CREATE TRIGGER planets_updated_on BEFORE UPDATE ON planets
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER stars_updated_on BEFORE UPDATE ON stars
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER stations_updated_on BEFORE UPDATE ON stations
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER regions_updated_on BEFORE UPDATE ON regions
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER constellations_updated_on BEFORE UPDATE ON constellations
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER planetary_systems_updated_on BEFORE UPDATE ON planetary_systems
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER planetary_system_jumps_updated_on BEFORE UPDATE ON planetary_system_jumps
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER factions_updated_on BEFORE UPDATE ON factions
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER corporations_updated_on BEFORE UPDATE ON corporations
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER corporation_investors_updated_on BEFORE UPDATE ON corporation_investors
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER moons_updated_on BEFORE UPDATE ON moons
     FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

CREATE TRIGGER ship_types_updated_on BEFORE UPDATE ON ship_types
FOR EACH ROW EXECUTE PROCEDURE all_update_updated_on();

-- Views

CREATE VIEW corporation_statistics AS
    SELECT
        c.name AS corporation_name,
        (
            SELECT
                COUNT(name)
            FROM stations
            WHERE corporation_name = c.name
        ) AS station_count
    FROM corporations AS c;

CREATE VIEW connected_regions AS
    SELECT r1.name AS region_name, r2.name AS connected_region_name FROM regions AS r1
        INNER JOIN constellations AS c1 ON (c1.region_name = r1.name)
        INNER JOIN planetary_systems AS ps1 ON (c1.name = ps1.constellation_name)
        INNER JOIN planetary_system_jumps AS psj ON (psj.to_planetary_system_name = ps1.name)
        INNER JOIN planetary_systems AS ps2 ON (ps2.name = psj.from_planetary_system_name)
        INNER JOIN constellations AS c2 ON (c2.name = ps2.constellation_name)
        INNER JOIN regions AS r2 ON (r2.name = c2.region_name AND r2.name != r1.name)
    GROUP BY r1.name, r2.name;

CREATE VIEW connected_constellations AS
    SELECT c1.name AS constellation_name, c2.name AS connected_constellation_name FROM constellations AS c1
        INNER JOIN planetary_systems AS ps1 ON (c1.name = ps1.constellation_name)
        INNER JOIN planetary_system_jumps AS psj ON (psj.to_planetary_system_name = ps1.name)
        INNER JOIN planetary_systems AS ps2 ON (ps2.name = psj.from_planetary_system_name)
        INNER JOIN constellations AS c2 ON (c2.name = ps2.constellation_name AND c2.name != c1.name)
    GROUP BY c1.name, c2.name;

CREATE VIEW station_planetary_systems AS
    SELECT s.name AS station_name, stations_get_planetary_system_name(s.name) AS planetary_system_name FROM stations AS s;

-- Testing data

/*BEGIN TRANSACTION;

SET CONSTRAINTS ALL DEFERRED;

INSERT INTO corporations
    (name, planetary_system_name, faction_name, friend_corporation_name, enemy_corporation_name)
    VALUES
    ('First', 'Sol', 'Terrans', 'Second', 'Third'),
    ('Second', 'Sol', 'Terrans', 'First', 'Third'),
    ('Third', 'Sol', 'Terrans', 'Second', 'First');

INSERT INTO corporation_investors
    (corporation_name, investor_corporation_name)
    VALUES
    ('First', 'Second');

INSERT INTO factions
    (name, planetary_system_name, corporation_name, military_corporation_name, race_name)
    VALUES
    ('Terrans', 'Sol', 'First', 'Second', 'Human');

INSERT INTO regions
    (name, faction_name)
    VALUES
    ('Alpha', 'Terrans');

INSERT INTO constellations
    (name, region_name, faction_name)
    VALUES
    ('Centauri', 'Alpha', 'Terrans');

INSERT INTO planetary_systems
    (name, constellation_name, sun_type_name, faction_name)
    VALUES
    ('Sol', 'Centauri', 'Hot', 'Terrans');

INSERT INTO stations
    (name, operation_name, planetary_system_name, corporation_name)
    VALUES
    ('Base', 'Military', 'Sol', 'Second');

COMMIT;

SELECT * FROM corporation_statistics;*/
