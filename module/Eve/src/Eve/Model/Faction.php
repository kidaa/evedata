<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\CorporationRepository")
 * @Table(name="factions")
 **/
class Faction extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="PlanetarySystem")
     * @JoinColumn(name="planetary_system_name", referencedColumnName="name")
     **/
    protected $planetary_system;

    /**
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="corporation_name", referencedColumnName="name")
     **/
    protected $corporation;

    /**
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="militia_corporation_name", referencedColumnName="name")
     **/
    protected $militia_corporation;

    /** @Column(type="string") **/
    protected $race_name;
    
    /** @Column(type="string") **/
    protected $description;

    /** @Column(type="float") **/
    protected $size_factor;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getPlanetarySystem()
    {
        return $this->planetary_system;
    }

    public function setPlanetarySystem(PlanetarySystem $planetary_system)
    {
        $this->planetary_system = $planetary_system;
    }

    public function getCorporation()
    {
        return $this->corporation;
    }

    public function setCorporation(Corporation $corporation)
    {
        $this->corporation = $corporation;
    }

    public function getMilitiaCorporation()
    {
        return $this->militia_corporation;
    }

    public function setMilitiaCorporation(Corporation $militia_corporation)
    {
        $this->militia_corporation = $militia_corporation;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getSizeFactor()
    {
        return $this->size_factor;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'planetary_system'],
                ['fieldName' => 'corporation'],
                ['fieldName' => 'militia_corporation'],
                ['fieldName' => 'description'],
                ['fieldName' => 'size_factor'],
            ]
        ];
    }

    public function __toString()
    {
        return '<' . $this->name . '>';
    }
}

?>
