<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\StarRepository")
 * @Table(name="stars")
 **/
class Star extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="PlanetarySystem")
     * @JoinColumn(name="planetary_system_name", referencedColumnName="name")
     **/
    protected $planetary_system;

    /** @Column(type="string") */
    protected $star_type_name;

    /**
     * @OneToMany(targetEntity="Planet", mappedBy="star")
     */
    protected $planets;

    /** @Column(type="integer") */
    protected $temperature;

    /** @Column(type="string") */
    protected $spectral_class;

    /** @Column(type="float") */
    protected $luminosity;

    /** @Column(type="float") */
    protected $age;

    /** @Column(type="float") */
    protected $life;

    /** @Column(type="float") **/
    protected $radius;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getPlanetarySystem()
    {
        return $this->planetary_system;
    }

    public function getStarTypeName()
    {
        return $this->star_type_name;
    }

    public function getPlanets()
    {
        return $this->planets;
    }

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function getSpectralClass()
    {
        return $this->spectral_class;
    }

    public function getLuminosity()
    {
        return $this->luminosity;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getLife()
    {
        return $this->life;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'planetary_system'],
                ['fieldName' => 'star_type_name'],
                ['fieldName' => 'planets'],
                ['fieldName' => 'temperature'],
                ['fieldName' => 'spectral_class'],
                ['fieldName' => 'luminosity'],
                ['fieldName' => 'age'],
                ['fieldName' => 'life'],
                ['fieldName' => 'radius'],
            ]
        ];
    }

    public function __toString()
    {
        return '<'
            . $this->name
            . ' - '
            . $this->planetary_system->getName()
            . ' - '
            . $this->planetary_system->getConstellation()->getName()
            . ' - '
            . $this->planetary_system->getConstellation()->getRegion()->getName()
            . '>';
    }
}