<?php

namespace Eve\Controller;

class MoonController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Moon',
            'route' => 'moon',
            'showRoute' => 'moon/show',
            'searchRoute' => 'moon/search',
            'indexRoute' => 'moon/index',
            'viewBasePath' => 'eve/moon/',
            'show' => array(
                'notFoundRouteName' => 'moon',
                'header' => 'eve/moon/show/header',
                'tabs' => array(
                    'locationTab' => array(
                        'partial' => 'eve/moon/show/location',
                        'tabName' => 'Location'
                    ),
                    'propertiesTab' => array(
                        'partial' => 'eve/moon/show/properties',
                        'tabName' => 'Properties'
                    ),
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
