<?php

namespace Eve\Controller;

class StarController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Star',
            'route' => 'star',
            'showRoute' => 'star/show',
            'searchRoute' => 'star/search',
            'indexRoute' => 'star/index',
            'viewBasePath' => 'eve/star/',
            'show' => array(
                'notFoundRouteName' => 'star',
                'header' => 'eve/star/show/header',
                'tabs' => array(
                    'propertiesTab' => array(
                        'partial' => 'eve/star/show/properties',
                        'tabName' => 'Properties'
                    ),
                    'planetsTab' => array(
                        'partial' => 'eve/star/show/planets',
                        'tabName' => 'Planets'
                    )
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
