<?php

namespace Eve\Controller;

class RegionController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Region',
            'route' => 'region',
            'showRoute' => 'region/show',
            'searchRoute' => 'region/search',
            'indexRoute' => 'region/index',
            'viewBasePath' => 'eve/region/',
            'show' => array(
                'notFoundRouteName' => 'region',
                'header' => 'eve/region/show/header',
                'tabs' => array(
                    'locationsTab' => array(
                        'partial' => 'eve/standard/show/locations',
                        'tabName' => 'Locations'
                    ),
                    'constellationsTab' => array(
                        'partial' => 'eve/region/show/constellations',
                        'tabName' => 'Constellations'
                    ),
                    'connectedToTab' => array(
                        'criteria' => function ($entity) { return \count($entity->getConnectedRegions()) > 0; },
                        'partial' => 'eve/region/show/connected-to',
                        'tabName' => 'Connected to'
                    ),
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
