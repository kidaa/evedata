<?php

namespace Eve\Controller;

use Zend\Mvc\Controller\AbstractActionController;

abstract class BaseController extends AbstractActionController
{
    protected $em = null;
    
    public function __construct()
    {   
        $this->em = \Eve\Module::getEntityManager();
    }
}
    
?>
