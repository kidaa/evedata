<?php

namespace Eve\Controller;

class CorporationController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Corporation',
            'route' => 'corporation',
            'showRoute' => 'corporation/show',
            'searchRoute' => 'corporation/search',
            'indexRoute' => 'corporation/index',
            'viewBasePath' => 'eve/corporation/',
            'show' => array(
                'notFoundRouteName' => 'station',
                'header' => 'eve/corporation/show/header',
                'tabs' => array(
                    'propertiesTab' => array(
                        'partial' => 'eve/corporation/show/properties',
                        'tabName' => 'Properties'
                    ),
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
