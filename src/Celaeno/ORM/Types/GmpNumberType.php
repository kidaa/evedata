<?php

namespace Celaeno\ORM\Types;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Celaeno\Util;

class GmpNumberType extends Type
{
    const SQL_TYPE = 'DOUBLE PRECISION';

    const NAME = 'gmp_number';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::SQL_TYPE;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null)
        {
            return null;
        }

        $value = Util::convertScientificNotationToDecimal($value);

        return \gmp_init($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return \gmp_strval($value);
    }

    public function getName()
    {
        return self::NAME;
    }

    public static function registerType(Connection $conn)
    {
        Type::addType(self::NAME, '\Celaeno\ORM\Types\GmpNumberType');
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping(self::NAME, self::NAME);
    }
}