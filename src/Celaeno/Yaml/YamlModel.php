<?php

namespace Celaeno\Yaml;

use Zend\View\Model\ViewModel;

class YamlModel extends ViewModel
{
    protected $terminate = true;

    protected $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function serialize()
    {
        if (!($this->object instanceof \Serializable))
        {
            throw new \Exception('Object is not an instance of Serializable.');
        }

        return \yaml_emit($this->object->serialize());
    }
}
