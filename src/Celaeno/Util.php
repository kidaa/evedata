<?php

namespace Celaeno;

class Util
{
    public static function methodNameToCamelCase($name)
    {
        return \str_replace(' ', '', \ucwords(\str_replace('_', ' ', $name)));
    }

    public static function convertScientificNotationToDecimal($value)
    {
        $value = str_replace('.', '', $value);

        $e_pos = strpos($value, 'e+');

        if (false !== $e_pos)
        {
            $e_value = intval(substr($value, $e_pos + 2));

            $str_value = substr($value, 0, $e_pos);

            return str_pad($str_value, $e_value + 1, '0');
        }

        return $value;
    }

    public static function formatNumber($number, $decimals = 0)
    {
        if (is_numeric($number))
        {
            return number_format($number, $decimals, '.', ',');
        }
        else if (\get_resource_type($number) == 'GMP integer')
        {
            return self::formatWholeNumberString(gmp_strval($number));
        }
        else
        {
            return 'Unknown number type.';
        }
    }

    /**
     * Dont look at this! YOU HAVE BEEN WARNED!
     *
     * @param $number
     * @param string $thousand_sep
     * @return string
     */
    public static function formatWholeNumberString($number, $thousand_sep = ',')
    {
        $length = strlen($number);

        $formatted_value = '';

        for (
            $i = 0;
            $i < $length;
            $i += ($offset))
        {
            $offset = ($i == 0) ? $length % 3 : 3;

            if ($offset == 0) $offset = 3;

            $formatted_value .= substr($number, $i, $offset);

            if ($i + 3 != $length)
            {
                $formatted_value .= $thousand_sep;
            }
        }

        return $formatted_value;
    }
}
