<?php

namespace Celaeno\Form;

class TextWidget extends Widget
{
    public function __construct($name)
    {
        parent::__construct('text-widget', $name);

        $this->setDefault('Noppes');
    }
}
