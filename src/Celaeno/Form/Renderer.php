<?php

namespace Celaeno\Form;

use Eve\Module;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\AggregateResolver;
use Zend\View\Resolver\TemplatePathStack;

class Renderer
{
    protected static $singletons = array();

    /** @var PhpRenderer */
    protected $phpRenderer;

    protected $directory;

    protected function __construct($directory)
    {
        $this->directory = $directory;
        $this->phpRenderer = new PhpRenderer();

        $resolver = new AggregateResolver();

        $this->phpRenderer->setResolver($resolver);

        $path = Module::getBaseDirectory() . $this->directory;

        $stack = new TemplatePathStack(
            array(
                 'script_paths' => array(
                     $path
                 )
            ));

        $resolver->attach($stack);
    }

    public static function getInstance($directory)
    {
        if (!isset(self::$singletons[$directory]))
        {
            self::$singletons[$directory] = new self($directory);
        }

        return self::$singletons[$directory];
    }

    public function render($variables, $template)
    {
        $this->phpRenderer->setVars($variables);

        $result = $this->phpRenderer->render($template);

        $this->phpRenderer->setVars(array());

        return $result;
    }
}
