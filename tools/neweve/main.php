<?php

echo "EVE DBO -> New EVE Migrator Tool by Florian.\n";

chdir(dirname(__DIR__) . '/..');

// Setup autoloading

echo "Setting up autoloading...\n";

require_once 'init_autoloader.php';

require_once __DIR__ . '/migrators/BaseMigrator.php';

Zend\Mvc\Application::init(require 'config/application.config.php');

function neweve_autoload($name)
{
    require_once __DIR__ . '/migrators/' . $name . '.' . 'php';
}

spl_autoload_register('neweve_autoload');

// Setup error handling

function exception_error_handler($severity, $message, $filename, $lineno)
{
    throw new \ErrorException($message, 0, $severity, $filename, $lineno);
}

set_error_handler('exception_error_handler');

// Handle arguments

$options = getopt(
    '',
    array(
         'migrate-only::',
         'dont-migrate::',
         'migrate-all::',
         'create-tables::',
    )
);



$migrators = array(
    'Corporations',
    'CorporationInvestors',
    'Factions',
    'Regions',
    'Constellations',
    'PlanetarySystems',
    'PlanetarySystemJumps',
    'Stars',
    'Planets',
    'Moons',
    'Stations',
    'ShipTypes',
    'Items',
    'Ships',
    'AttributeTypes',
    'Attributes',
    'Skills',
    'SkillAttributes'
);

if (isset($options['migrate-only']))
{
    $migrators = explode(',', $options['migrate-only']);
}
else if (isset($options['dont-migrate']))
{
    $migrators = array_diff($migrators, explode(',', $options['dont-migrate']));
}
else if (isset($options['migrate-all']))
{
    // ...
}
else
{
    echo 'You must explicitly specify one of the following: ' . PHP_EOL;

    echo ' --migrate-all' . PHP_EOL;
    echo ' --migrate-only=Migrator1,Migrator2,...' . PHP_EOL;
    echo ' --dont-migrate=Migrator1,Migrator2,...' . PHP_EOL;

    exit(0);
}

echo 'The following migrators are run: ' . PHP_EOL;

$new_migrators = array();

foreach ($migrators as $migrator)
{
    echo '- ' . $migrator . PHP_EOL;

    $new_migrators[] = $migrator;
}

$migrators = $new_migrators;

// Setup databases

echo "Connecting to databases...\n";

$eve_connection = \Eve\Module::getDatabaseConfig()['eve_connection'];

$evedbo_conn = new PDO(
    $eve_connection['dsn'],
    $eve_connection['user'],
    $eve_connection['password']
);

$connection = \Eve\Module::getDatabaseConfig()['connection'];

$neweve_conn = pg_connect(
    'host=' . $connection['host'] . ' ' .
    'user=' . $connection['user'] . ' ' .
    'password=' . $connection['password'] . ' ' .
    'dbname=neweve'
);

echo "Successfully connected to databases.\n";

// Do actual migrations

echo "Beginning transaction...\n";

pg_query($neweve_conn, "BEGIN TRANSACTION");

pg_query($neweve_conn, "SET CONSTRAINTS ALL DEFERRED");

$failure = false;

$result_now = pg_query($neweve_conn, "SELECT NOW()");

$now = pg_fetch_row($result_now)[0];

echo "Transaction starting time: " . $now . PHP_EOL;

if (isset($options['create-tables']) && !$options['create-tables'])
{
    $source_sql_filename = '/sql/neweve.sql';

    echo 'Creating all tables...' . PHP_EOL;

    $source_sql = file_get_contents(getcwd() . $source_sql_filename);

    $failure = !pg_query($neweve_conn, $source_sql);
}
else
{
    echo "Truncating selected tables...\n";

    $truncate_query = 'TRUNCATE TABLE ';

    for ($i = 0; $i < count($migrators); $i++)
    {
        $migrator_name = $migrators[$i];

        $truncate_query .= $migrator_name::TABLE_NAME;

        if ($i != (count($migrators) - 1))
            $truncate_query .= ', ';
    }

    $truncate_query .= ' CASCADE';

    $faileure = !pg_query($neweve_conn, $truncate_query);
}

if (!$failure)
{
    echo "Starting migrators...\n";

    $total_execution_time = microtime(true);

    foreach ($migrators as $migrator_name)
    {
        echo "=== STARTING MIGRATOR: " . $migrator_name . " ===\n";

        echo "Table name: " . $migrator_name::TABLE_NAME . PHP_EOL;

        $start_time = microtime(true);

        try
        {
            echo "Attempting migrator instantiation...\n";

            $migrator = new $migrator_name($now, $neweve_conn, $evedbo_conn);
        }
        catch (\Exception $exception)
        {
            echo $migrator_name . ": INSTANTIATION FAILED: " . $exception->getMessage() . PHP_EOL;
            $failure = true;
            break;
        }

        try
        {
            echo "Attempting migrator setup...\n";

            $migrator->setup();
        }
        catch (\Exception $exception)
        {
            echo $migrator_name . ": SETUP FAILED: " . $exception->getMessage() . PHP_EOL;
            $failure = true;
            break;
        }

        try
        {
            echo "Attempting actual migration...\n";

            $migrator->processMigration();

            echo "Migrator affected " . $migrator->getAffectedRows() . " row(s).\n";
        }
        catch (\Exception $exception)
        {
            echo $migrator_name . ": MIGRATION FAILED: " . $exception->getMessage() . PHP_EOL;
            $failure = true;
            break;
        }

        try
        {
            echo "Attempting migrator teardown...\n";

            $migrator->teardown();
        }
        catch (\Exception $exception)
        {
            echo $migrator_name . ": TEARDOWN FAILED: " . $exception->getMessage() . PHP_EOL;
            $failure = true;
            break;
        }

        $execution_time = microtime(true) - $start_time;

        echo "Migrator took " . number_format($execution_time, 3) . " second(s).\n";
        echo "=== ENDING MIGRATOR: " . $migrator_name . " ===\n";
    }

    $total_execution_time = microtime(true) - $total_execution_time;

    echo "Total execution time: " . number_format($total_execution_time, 3) . " second(s)." . PHP_EOL;

    echo "Done with migrators.\n";
}

if ($failure)
{
    echo "DETECTED FAILURE STATE, ROLLING BACK!\n";

    pg_query($neweve_conn, "ROLLBACK");
}
else
{
    echo "No failures detected, committing...\n";

    pg_query($neweve_conn, "COMMIT");
}

echo "This tool is done, exiting.\n";
