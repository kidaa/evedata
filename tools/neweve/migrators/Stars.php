<?php

class Stars extends BaseMigrator
{
    const TABLE_NAME = 'stars';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                n.itemName AS name,
                n.itemID AS original_id,
                nss.itemName AS planetary_system_name,
                nst.typeName AS star_type_name,
                cs.temperature,
                cs.spectralClass AS spectral_class,
                cs.luminosity,
                cs.age,
                cs.life,
                cs.radius
            FROM mapDenormalize AS dn
                LEFT JOIN invNames AS n ON (n.itemID = dn.itemID)
                LEFT JOIN invNames AS nss ON (nss.itemID = dn.solarSystemID)
                LEFT JOIN mapSolarSystems AS ss ON (ss.solarSystemID = dn.solarSystemID)
                LEFT JOIN invTypes AS nst ON (nst.typeID = ss.sunTypeID)
                LEFT JOIN mapCelestialStatistics AS cs ON (cs.celestialID = dn.itemID)
            WHERE dn.groupID = 6
        ');

        $this->beginCopyTo('stars');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['name'],
                $row['original_id'],
                $row['planetary_system_name'],
                $row['star_type_name'],
                (int) $row['temperature'],
                $row['spectral_class'],
                $row['luminosity'],
                $row['age'],
                $row['life'],
                $row['radius'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
