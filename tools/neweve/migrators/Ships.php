<?php

class Ships extends BaseMigrator
{
    const TABLE_NAME = 'ships';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                t.typeName AS ship_name,
                g.groupName AS ship_type_name
            FROM invTypes AS t
                INNER JOIN invGroups AS g ON (g.groupID = t.groupID)
                INNER JOIN invCategories AS c ON (c.categoryID = g.categoryID)
            WHERE c.categoryID = 6 AND t.marketGroupID IS NOT NULL;
        ');

        $this->beginCopyTo('ships');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['ship_name'],
                $row['ship_type_name'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
