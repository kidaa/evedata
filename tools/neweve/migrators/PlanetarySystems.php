<?php

class PlanetarySystems extends BaseMigrator
{
    const TABLE_NAME = 'planetary_systems';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                ss.solarSystemID AS original_id,
                ss.solarSystemName,
                ss.x,
                ss.y,
                ss.z,
                ss.xMin,
                ss.xMax,
                ss.yMin,
                ss.yMax,
                ss.zMin,
                ss.zMax,
                ss.luminosity,
                ss.security,
                ss.radius,
                ss.securityClass,
                f.factionName AS faction_name,
                t.typeName AS sun_type_name,
                c.itemName AS constellation_name
            FROM mapSolarSystems AS ss
                LEFT JOIN invNames AS c ON (c.itemID = ss.constellationID)
                LEFT JOIN invTypes AS t ON (t.typeID = ss.sunTypeID)
                LEFT JOIN chrFactions AS f ON (f.factionID = ss.factionID)
        ');

        $this->beginCopyTo('planetary_systems');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['solarSystemName'],
                $row['original_id'],
                $row['constellation_name'],
                $row['sun_type_name'],
                null, // No data for this in evedbo
                $row['x'],
                $row['y'],
                $row['z'],
                $row['xMin'],
                $row['xMax'],
                $row['yMin'],
                $row['yMax'],
                $row['zMin'],
                $row['zMax'],
                $row['luminosity'],
                $row['security'],
                $row['radius'],
                $row['securityClass'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
