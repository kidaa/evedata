<?php

class Items extends BaseMigrator
{
    const TABLE_NAME = 'items';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                t.typeName AS item_name,
                t.typeID AS original_id,
                t.description,
                t.mass,
                t.volume,
                t.capacity,
                t.portionSize AS portion_size,
                r.raceName AS race_name,
                t.basePrice AS base_price,
                t.published,
                m.marketGroupName AS market_group_name,
                t.chanceOfDuplicating AS chance_of_duplicating
            FROM invTypes AS t
                LEFT JOIN chrRaces AS r ON (r.raceID = t.raceID)
                INNER JOIN invMarketGroups AS m ON (m.marketGroupID = t.marketGroupID)
            WHERE t.marketGroupID IS NOT NULL
        ');

        $this->beginCopyTo('items');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['item_name'],
                $row['original_id'],
                $row['race_name'],
                $row['market_group_name'],
                $row['description'],
                $row['mass'],
                $row['volume'],
                $row['capacity'],
                $row['portion_size'],
                $row['base_price'],
                $row['published'],
                $row['chance_of_duplicating'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
