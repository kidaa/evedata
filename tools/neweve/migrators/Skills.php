<?php

class Skills extends BaseMigrator
{
    const TABLE_NAME = 'skills';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                invTypes.typeName AS item_name,
                dgmTypeAttributes.valueFloat AS rank
            FROM invTypes
                INNER JOIN invGroups ON (invGroups.groupID = invTypes.groupID)
                INNER JOIN invCategories ON (invCategories.categoryID = invGroups.categoryID)
                INNER JOIN dgmTypeAttributes ON (dgmTypeAttributes.typeID = invTypes.typeID AND dgmTypeAttributes.attributeID = 275)
            WHERE invTypes.marketGroupID IS NOT NULL AND invCategories.categoryID = 16
        ');

        $this->beginCopyTo('skills');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['item_name'],
                (int) $row['rank'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
