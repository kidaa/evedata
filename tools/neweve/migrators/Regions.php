<?php

class Regions extends BaseMigrator
{
    const TABLE_NAME = 'regions';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                r.regionID AS original_id,
                r.regionName,
                r.x,
                r.y,
                r.z,
                r.xMin,
                r.xMax,
                r.yMin,
                r.yMax,
                r.zMin,
                r.zMax,
                r.radius,
                f.itemName AS faction_name
            FROM mapRegions AS r
                LEFT JOIN invNames AS f ON (f.itemID = r.factionID)
        ');

        $this->beginCopyTo('regions');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['regionName'],
                $row['original_id'],
                $row['faction_name'],
                $row['x'],
                $row['y'],
                $row['z'],
                $row['xMin'],
                $row['xMax'],
                $row['yMin'],
                $row['yMax'],
                $row['zMin'],
                $row['zMax'],
                $row['radius'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
