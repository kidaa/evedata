<?php

class CorporationInvestors extends BaseMigrator
{
    const TABLE_NAME = 'corporation_investors';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                cn.itemName AS corporationname,
                c.investorShares1,
                c.investorShares2,
                c.investorShares3,
                c.investorShares4,
                c1n.itemName as investor_corporation_name1,
                c2n.itemName as investor_corporation_name2,
                c3n.itemName as investor_corporation_name3,
                c4n.itemName as investor_corporation_name4
            FROM crpNPCCorporations AS c
                LEFT JOIN invNames AS cn ON (cn.itemID = c.corporationID)
                LEFT JOIN invNames AS c1n ON (c1n.itemID = c.investorID1)
                LEFT JOIN invNames AS c2n ON (c2n.itemID = c.investorID2)
                LEFT JOIN invNames AS c3n ON (c3n.itemID = c.investorID3)
                LEFT JOIN invNames AS c4n ON (c4n.itemID = c.investorID4)
            WHERE investorShares1 != 0 AND investorShares2 != 0 AND investorShares3 != 0 AND investorShares4 != 0
            ORDER BY corporationname ASC
        ');

        $this->beginCopyTo('corporation_investors');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            // TODO: could be a for loop ya know! *slams head on desk*
            $copy_row1 = array(
                $row['corporationname'],
                $row['investor_corporation_name1'],
                $row['investorShares1'],
                $this->pgNow(),
                null
            );

            $copy_row2 = array(
                $row['corporationname'],
                $row['investor_corporation_name2'],
                $row['investorShares2'],
                $this->pgNow(),
                null
            );

            $copy_row3 = array(
                $row['corporationname'],
                $row['investor_corporation_name3'],
                $row['investorShares3'],
                $this->pgNow(),
                null
            );

            $copy_row4 = array(
                $row['corporationname'],
                $row['investor_corporation_name4'],
                $row['investorShares4'],
                $this->pgNow(),
                null
            );

            if ($row['investor_corporation_name1'])
                $this->copyRow($copy_row1);

            if ($row['investor_corporation_name2'])
                $this->copyRow($copy_row2);

            if ($row['investor_corporation_name3'])
                $this->copyRow($copy_row3);

            if ($row['investor_corporation_name4'])
                $this->copyRow($copy_row4);
        }

        $this->endCopyTo();
    }
}
