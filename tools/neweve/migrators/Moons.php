<?php

class Moons extends BaseMigrator
{
    const TABLE_NAME = 'moons';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                n.itemName AS name,
                n.itemID AS original_id,
                dn.x,
                dn.y,
                dn.z,
                cs.temperature,
                cs.radius,
                cs.orbitRadius AS orbit_radius,
                cs.orbitPeriod AS orbit_period,
                cs.eccentricity,
                cs.massDust AS mass_dust,
                cs.massGas AS mass_gas,
                cs.density,
                cs.surfaceGravity AS surface_gravity,
                cs.escapeVelocity AS escape_velocity,
                cs.rotationRate AS rotation_rate,
                cs.locked,
                cs.pressure,
                dn.orbitIndex AS orbit_position,
                dns.itemName AS planet_name
            FROM mapDenormalize AS dn
                INNER JOIN invNames AS n ON (n.itemID = dn.itemID)
                INNER JOIN mapDenormalize dns ON (dns.itemID = dn.orbitID)
                LEFT JOIN mapCelestialStatistics AS cs ON (cs.celestialID = dn.itemID)
            WHERE dn.groupID = 8
        ');

        $this->beginCopyTo('moons');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['name'],
                $row['original_id'],
                $row['planet_name'],
                $row['x'],
                $row['y'],
                $row['z'],
                $row['radius'],
                $row['temperature'],
                $row['orbit_radius'],
                $row['orbit_period'],
                $row['eccentricity'],
                $row['mass_dust'],
                $row['mass_gas'],
                $row['density'],
                $row['surface_gravity'],
                $row['escape_velocity'],
                $row['rotation_rate'],
                $row['locked'],
                $row['pressure'],
                $row['orbit_position'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
